function sortPhraseKeys(phrases) {
  return Object.keys(phrases).sort((a,b) => phrases[b] - phrases[a]);
}

function getReportList(db, count) {
  const report = [];
  let idx = 0;
  sortPhraseKeys(db.phrases).slice(0,count).forEach((key) => {
    const phraseCount = db.phrases[key];
    report[idx++] = [key, phraseCount];
  })
  return report;
}

function addPhrase(db, phrase) {
  if (!(phrase in db.phrases)) {
    db.phrases[phrase] = 0
    return `Phrase added: ${phrase}`;
  }
}

module.exports = {
  getReportList,
  sortPhraseKeys,
  addPhrase,
}
