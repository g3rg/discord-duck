const Discord = require('discord.js');
const { prefix } = require('../config.json');

module.exports = {
  name: 'help',
  cooldown: 5,
  description: 'Display command list and other information',
  execute(message, args, db) {
    const report = new Discord.MessageEmbed().setTitle('Help');
    const { commands } = message.client;
    const data = [];

    if (!args.length) {
      data.push(commands.map(command => command.name).join('\n'));
      data.push(`\nYou can ask for help on a command with '${prefix}help [command name]'`)
    } else {
      const commandName = args[0];
      const command = commands.get(commandName);

      if (!command) {
        data.push(`Command '${commandName} does not exist`);
      } else {
        if (command.usage) {
          data.push(`${commandName} - ${command.description}\n`)
          data.push(`${prefix}${commandName} ${command.usage}`)
        } else {
          data.push('Command has no further help information');
        }
      }
    }

    report.setDescription(data);
    message.channel.send(report);
  }
}