const { prefix } = require('../config.json');

function getPhrase(message, commandLength) {
  return message.content.slice(prefix.length + commandLength).trim();
}

module.exports = {
  name: 'remregex',
  cooldown: 5,
  description: 'Removes a regex',
  usage: '{phrase}',
  execute(message, args, db) {
    const phrase = getPhrase(message, 'remregex'.length);
    console.log(phrase);
    console.log(db);
    if (phrase in db.regexes) {
      delete db.regexes[phrase];
      message.channel.send(`Regex removed: ${phrase}`);
    } else {
      message.channel.send("Regex doesn't exist");
    }
  }
}
