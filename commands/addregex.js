const { prefix } = require('../config.json');

function getPhrase(message, commandLength) {
  return message.content.slice(prefix.length + commandLength).trim();
}

module.exports = {
  name: 'addregex',
  cooldown: 5,
  description: 'Add a regex to auto add phrases',
  usage: '{regex}',
  execute(message, args, db) {
    const phrase = getPhrase(message, 'addregex'.length);
    if (phrase in db.regexes) {
      message.channel.send(`Regex already exists: ${phrase}`);
    } else {
      db.regexes[phrase] = 0
      message.channel.send(`Regex added: ${phrase}`);
    }
  }
}
