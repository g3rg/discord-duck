const Discord = require('discord.js');
const { getReportList } = require('../phrase.js');

module.exports = {
  name: 'report',
  cooldown: 5,
  description: 'Display a report of current phrases and counts',
  usage: '[all|x] - Show top 5 phrases, or optionally [all] or a set [number] of phrases',
  execute(message, args, db) {
    let report = '';
    let count = 5;

    if (args.length != 0) {
      if (args[0].toLowerCase().trim() === 'all') {
        count = Object.keys(db.phrases).size;
      } else if (!isNaN(args[0].trim()) && Number.isInteger(parseFloat(args[0].trim()))) {
        count = parseInt(args[0].trim())
      }
    }

    const reportEmbed = new Discord.MessageEmbed().setTitle('Top Phrases');
    getReportList(db, count).forEach((val,idx) => {
      report += `${val[0]}: ${val[1]}\n`;
    });
    reportEmbed.setDescription(report);
    message.channel.send(reportEmbed);
  },
}
