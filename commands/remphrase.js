const { prefix } = require('../config.json');

function getPhrase(message, commandLength) {
  return message.content.slice(prefix.length + commandLength).trim().toLowerCase();
}

module.exports = {
  name: 'remphrase',
  cooldown: 5,
  description: 'Removes a phrase',
  usage: '{phrase}',
  execute(message, args, db) {
    const phrase = getPhrase(message, 'remphrase'.length);

    if (phrase in db.phrases) {
      delete db.phrases[phrase];
      message.channel.send(`Phrase removed: ${phrase}`);
    } else {
      message.channel.send("Phrase doesn't exist");
    }
  }
}