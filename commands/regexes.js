const Discord = require('discord.js');

module.exports = {
  name: 'regexes',
  cooldown: 5,
  description: 'List all current regexes being monitored',
  usage: '',
  execute(message, args, db) {
    let report = '';

    const reportEmbed = new Discord.MessageEmbed().setTitle('Regexes');
    for (regex in db.regexes) {
      report += `${regex}\n`;
    }
    reportEmbed.setDescription(report);
    message.channel.send(reportEmbed);
  },
}
