const Discord = require('discord.js');
const { ChartJSNodeCanvas } = require('chartjs-node-canvas');
const { getReportList } = require('../phrase.js');

const width = 800;
const height = 600;

const plugin = {
  id: 'custom_canvas_background_color',
  beforeDraw: (chart) => {
    const ctx = chart.canvas.getContext('2d');
    ctx.save();
    ctx.globalCompositeOperation = 'destination-over';
    ctx.fillStyle = '#222222';
    ctx.fillRect(0, 0, chart.width, chart.height);
    ctx.restore();
  }
};

module.exports = {
  name: 'pie',
  cooldown: 5,
  description: 'Build a pie chart to show the breakdown of phrase counts in a pretty format',
  usage: '[all] - Show top 5 phrases + the rest, or optionally [all] phrases',
  execute: async (message, args, db) => {
    let count = 5;

    const canvas = new ChartJSNodeCanvas( {width, height });
    const labels = [];
    const counts = [];
    let etcCount = 0;

    getReportList(db, Object.keys(db.phrases).size).forEach((val,idx) => {
      if (count > 0) {
        labels.push(val[0]);
        counts.push(val[1]);
        count--;
      } else {
        etcCount++;
      }
    });

    labels.push('The Rest');
    counts.push(etcCount);

    const data = {
      labels: labels,
      datasets: [
        {
          label: 'Phrases',
          data: counts,
          backgroundColor: ['red', 'orange', 'yellow', 'green', 'blue', 'white'],
        }
      ]
    }

    const config = {
      type: 'pie',
      data: data,
      plugins: [plugin],
      options: {
        plugins: {
          legend: {
            display: true,
            position: 'top',
            color: 'red',
          },
        }
      },
    };

    const image = await canvas.renderToBuffer(config);
    const attachment = new Discord.MessageAttachment(image);
    await message.channel.send(attachment);
  },
}
