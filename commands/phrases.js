const Discord = require('discord.js');

module.exports = {
  name: 'phrases',
  cooldown: 5,
  description: 'List all current phrases being monitored',
  usage: '',
  execute(message, args, db) {
    let report = '';


    const reportEmbed = new Discord.MessageEmbed().setTitle('Phrases');
    for (phrase in db.phrases) {
      report += `${phrase}\n`;
    }
    reportEmbed.setDescription(report);
    message.channel.send(reportEmbed);
  },
}
