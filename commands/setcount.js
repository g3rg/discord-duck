const { prefix } = require('../config.json');

function getPhrase(message, commandLength) {
  return message.content.slice(prefix.length + commandLength).trim().toLowerCase();
}

module.exports = {
  name: 'setcount',
  cooldown: 5,
  description: 'Updates the count for a phrase',
  usage: '{count} {phrase}',
  execute(message, args, db) {
    const count = args[0];
    const phrase = getPhrase(message, 'setcount '.length + count.length).trim();

    if (phrase in db.phrases) {
      db.phrases[phrase] = count;
      message.channel.send(`Phrase count set to ${count}`);
    } else {
      message.channel.send('Phrase not found');
    }
  }
}