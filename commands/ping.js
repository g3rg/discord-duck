const Discord = require('discord.js');
const { prefix } = require('../config.json');

module.exports = {
  name: 'ping',
  description: 'Ping the bot server',
  cooldown: 5,
  execute(message, args) {
    message.channel.send('pong');
  }
}
