const { prefix } = require('../config.json');

function getPhrase(message, commandLength) {
  return message.content.slice(prefix.length + commandLength).trim().toLowerCase();
}

module.exports = {
  name: 'addphrase',
  cooldown: 5,
  description: 'Add a phrase to be tracked',
  usage: '{phrase}',
  execute(message, args, db) {
    console.log(args);
    const phrase = getPhrase(message, 'addphrase'.length);

    if (phrase in db.phrases) {
      message.channel.send(`Phrase already exists: ${phrase}`);
    } else {
      db.phrases[phrase] = 0
      message.channel.send(`Phrase added: ${phrase}`);
    }
  }
}