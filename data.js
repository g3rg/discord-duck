const fs = require('fs');

function loadDB(guildId, dbdir) {
  let rawdata = '{ "phrases": {}, "regexes": {} }';
  try {
    rawdata = fs.readFileSync(`${dbdir}/db-${guildId}.json`);
  } catch (err) {
    console.log('Error Loading DB:' + err);
  }
  return JSON.parse(rawdata);
}

function saveDB(guildId, db, dbdir) {
  let js = JSON.stringify(db);
  fs.writeFileSync(`${dbdir}//db-${guildId}.json`, js);
}

module.exports = {
  loadDB, saveDB
}
