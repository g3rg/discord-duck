FROM node:14
WORKDIR /usr/src/app
COPY package*.json ./

RUN apt-get --yes update
RUN apt-get --yes install build-essential libcairo2-dev libpango1.0-dev libjpeg-dev libgif-dev librsvg2-dev

RUN npm install

COPY . .

CMD ["node", "bot.js"]


