const fs = require('fs');
const Discord = require('discord.js');
const { loadDB, saveDB } = require('./data.js');

const { prefix, token, dbdir, mention_milestone } = require("./config.json");
const { sortPhraseKeys, addPhrase } = require("./phrase");

const client = new Discord.Client();

client.commands = new Discord.Collection();
const commandFiles = fs.readdirSync("./commands").filter(file=>file.endsWith('.js'));

client.cooldowns = new Discord.Collection();

for (const file of commandFiles) {
  const command = require(`./commands/${file}`);
  client.commands.set(command.name, command);
  console.log(`Loaded ${command.name}`);
}

client.once('ready', () => {
  console.log('Ready!');
});

client.on('message', message => {

  if (message.author.bot || !message.guild) return;   // Ignore bots and DMs

  const guildId = message.guild.id;
  const serverName = message.guild.name;
  const channelName = message.channel;

  const db = loadDB(message.guild.id, dbdir);

  if (message.content.startsWith(prefix)) {

    const args = message.content.slice(prefix.length).trim().split(/ +/);
    const commandName = args.shift().toLowerCase();

    if (!client.commands.has(commandName)) return;

    const command = client.commands.get(commandName);

    const { cooldowns } = client;

    if (!cooldowns.has(command.name)) {
      cooldowns.set(command.name, new Discord.Collection());
    }

    const now = Date.now();
    const timestamps = cooldowns.get(command.name);
    const cooldownAmount = (command.cooldown || 3) * 1000;

    if (timestamps.has(message.author.id)) {
      const expirationTime = timestamps.get(message.author.id) + cooldownAmount;

      if (now < expirationTime) {
        const timeLeft = (expirationTime - now) / 1000;
        return message.reply(`please wait ${timeLeft.toFixed(1)} more second(s) before reusing the \`${command.name}\` command.`);
      }
    }
    timestamps.set(message.author.id, now);
    setTimeout(() => timestamps.delete(message.author.id), cooldownAmount);


    try {
      command.execute(message, args, db);
    } catch (error) {
      console.error(error);
      message.reply('I had a problem with that command sorry');
    }

  } else {
    let foundPhrase = '';
    const pre = JSON.stringify(sortPhraseKeys(db.phrases));
    let phraseCount = 0;

    for (regex in db.regexes) {
      var pattern = new RegExp(regex, "i");
      var result = message.content.match(pattern);
      if (result) {
        console.log(`Res: ${result}`);
        var newPhrase = result[0].trim().toLowerCase();
        var result = addPhrase(db, newPhrase);
        if (result) {
          message.channel.send(result);
        }
      }
    }

    for (phrase in db.phrases) {
      var phraseRegex = new RegExp(`\\b${phrase}\\b`, 'i');
      var result = message.content.match(phraseRegex);

      if (result) {
        db.phrases[phrase]+= result.length;
        phraseCount = db.phrases[phrase];
        foundPhrase = phrase;
      }
    }
    const post = JSON.stringify(sortPhraseKeys(db.phrases));

    if (pre !== post) {
      message.channel.send(`"${foundPhrase}" has moved up a place!`);
    }
    if (foundPhrase !== "" && phraseCount % mention_milestone === 0) {
      message.channel.send(`"${foundPhrase}" has reached ${phraseCount} mentions!`);
    }
  }

  saveDB(guildId, db, dbdir);
});

client.login(token);
